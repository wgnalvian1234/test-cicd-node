FROM node:10-alpine

WORKDIR app

ARG PORT

ENV PORT $PORT

COPY package.json package-lock.json ./

RUN chown -R node:node /app

USER node

RUN npm install --unsafe-perm

COPY --chown=node:node . .

EXPOSE 4000

CMD ["node","app.js"]

